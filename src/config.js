export const PORT = 2323

export const MONGODB_OPTIONS = {
  database: 'mongodb://127.0.0.1:27017/nodejs-starter'
}

export const JWT_SECRET = 'JWTSECRET'
export const AUTH_API = 'http://localhost:1234'

export default {
  PORT,
  MONGODB_OPTIONS,
  JWT_SECRET,
  AUTH_API
}
