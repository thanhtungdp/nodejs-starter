import Menu from 'models/Menu'

export default {
  async checkExists ({ role, name }) {
    const menuExists = await Menu.findOne({ role, name })
    return menuExists
  },
  async createMenu ({ role, name, menuList = [] }) {
    const menu = new Menu({
      role,
      name,
      menuList
    })
    await menu.save()
    return menu
  },
  async getMenu ({ role, name = 'default' }) {
    const menu = await Menu.findOne({ role, name })
    return menu
  }
}
