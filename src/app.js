import bodyParser from 'body-parser'
import express from 'express'
import mongoose from 'mongoose'

import config from './config'
import menuRoute from './routes/menuRoute'

// Init app express
const app = express()

// Connect mongodb
mongoose.connect(config.MONGODB_OPTIONS.database)


app.use(function (req, res, next) {
    if (app.get('env') === 'development') {
        res.setHeader('Access-Control-Allow-Origin', '*');
    } else {
        res.setHeader('Access-Control-Allow-Origin', '*');
    }

    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Cache-Control, Authorization');
    res.setHeader('Access-Control-Allow-Credentials', true);

    next();
});

// Setup bodyparser
app.use(bodyParser.urlencoded({ extended: true, limit: '100mb' }))
app.use(bodyParser.json())

// Routes
app.get('/', (req, res) => {
  res.json({message: 'Hello world'})
})

app.use('/menu', menuRoute)

app.listen(config.PORT, () => {
  console.log(`start server on ${config.PORT}`)
})
