import mongoose from 'mongoose'

import config from './config'
import menuDao from './dao/menuDao'

mongoose.connect(config.MONGODB_OPTIONS.database)

async function seeding () {
  try {
    const menuExists = await menuDao.checkExists({role: 'user', name: 'home'})

    // Kiểm tra đã seeding trước đó hay chưa
    if (menuExists) {
      console.log('=== SEEDING EXISTS ===')
      return
    }

    const menuHome = {
      role: 'user',
      name: 'home',
      menuList: [
        { id: 'home', name: 'Home', url: '/home' },
        { id: 'about', name: 'About', url: '/about' }
      ]
    }

    const menuFooter = {
      role: 'user',
      name: 'footer',
      menuList: [
        { id: 'home', name: 'Home', url: '/home' },
        { id: 'about', name: 'About', url: '/about' }
      ]
    }

    await menuDao.createMenu(menuHome)
    await menuDao.createMenu(menuFooter)

    console.log('=== SEEDING COMPLETE ===')
  } catch (e) {
    console.log(e)
  }
}

seeding()
